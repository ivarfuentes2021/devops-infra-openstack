
variable "image" {
  default = ""
  type = string
}

variable "flavor" {
  default = ""
  type = string
}

variable "ssh_key_file" {
  default = ""
  type = string
}

variable "instance_prefix" {
  default = ""
  type = string
}


variable "key_pair" {
  default = ""
  type = string
}

variable "security_group" {
  default = ""
  type = string
}
