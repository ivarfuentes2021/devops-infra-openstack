# Define required providers
terraform {
  required_version = ">= 1.0.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.42.0"
    }
  }
}

# terraform {
#   backend "swift" {
#     container         = "terraform-state"
#     archive_container = "terraform-state-archive"
#       state_name = "terraform.tfstate"
#   }
# }

data "template_file" "user_data" {
  template = file("../scripts/install-gocd.yml")
}

# Create GoCD Server.
resource "openstack_compute_instance_v2" "gocd_server" {
  name            = "${var.instance_prefix}GOCD01"
  image_name      = var.image
  flavor_name     = var.flavor
  key_pair        = var.key_pair
  security_groups = ["default", var.security_group]
  user_data       = data.template_file.user_data.rendered

  block_device {
    uuid                  = data.openstack_images_image_v2.ubuntu.id
    source_type           = "image"
    destination_type      = "volume"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }
}

data "openstack_images_image_v2" "ubuntu" {
  name        = var.image
  most_recent = true
}
